package main

import (
  "flag"
  "os"
  "fmt"
  "encoding/csv"
)

func main()  {
  csvFileName := flag.String("csv", "questions.csv", "a csv file containing lines in the format 'question,answer'")
  flag.Parse()

  file, err := os.Open(*csvFileName)
  if err != nil {
    exit(fmt.Sprintf("Failed to open the csv file: %s", *csvFileName))
  }

  reader := csv.NewReader(file)
  lines, err := reader.ReadAll()
  if err != nil {
    exit("Failed to parse the csv file")
  }

  questions := parseQuestions(lines)
  correct := 0
  for i, question := range questions {
    fmt.Printf("Question #%d: %s\n", i + 1, question.question)
    var answer string
    fmt.Scanf("%s\n", &answer)
    if answer == question.answer {
      correct++
      fmt.Println("Correct")
    }
  }

  fmt.Printf("Your score is %d out of %d\n", correct, len(questions))
}

func parseQuestions(lines [][]string) []question {
  questions := make([]question, len(lines))
  for i, line := range lines {
    questions[i] = question{
      question: line[0],
      answer: line[1],
    }
  }
  return questions
}

type question struct {
  question string
  answer string
}

func exit(msg string) {
  fmt.Println(msg)
  os.Exit(1)
}
